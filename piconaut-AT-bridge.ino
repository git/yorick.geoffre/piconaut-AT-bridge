#include <Arduino.h>

#define MAX_LINE_LENGTH 256
char lineBuffer[MAX_LINE_LENGTH];
uint8_t bufferPosition = 0;

void setup() {
  // put your setup code here, to run once:
  Serial1.setTX(12);
  Serial1.setRX(13);
  Serial1.begin(115200);
  Serial.begin(115200);
  delay(2000);
}

void loop() {
  // if data is available on Serial1, read it and write it to Serial
  if (Serial1.available()) {
    int incomingByte = Serial1.read();
    Serial.write(incomingByte);
  }

  if (Serial.available()) {
    int incomingByte = Serial.readLine();
    Serial1.write(incomingByte);
  }
}